-------------------------------------------------------------------------------
REVELATIONS VIII: SOME HOMOLOGY COMPUTATIONS
-------------------------------------------------------------------------------

This is going to be some elementary homology computations and/or an Erlang
library for doing homology computations

A simplicial complex would be an object-like thing (a data structure, and it
validates along the way) basically.

Need to move the cw data structure code outside to its own module
when I come back, work on separating that out

Ah, it should all be simplicial: because simplices have the property that an
n-simplex has n+1 faces)

%-spec defsimplex(Name :: atom()) -> Result
%    when Result :: ok | {error, name_taken}.

-spec defvertex(Name :: atom()) -> Result
    when Result :: ok | {error, name_taken}.

-spec defedge(Name :: atom(), Source :: atom(), Destination :: atom()) -> Result
    when Result :: ok
                 | {error, Reason},
         Reason :: name_taken
                 | boundary_does_not_exist.

-spec defface(Name :: atom(), Boundary) -> Result
    when Result :: ok
                 | {error, Reason},
         Reason :: name_taken
                 | boundary_does_not_exist
                 | boundary_is_not_cycle.
