% @doc
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qq_q: rational arithmetic
%
% This module contains the implementation of the q() type, which is rational
% numbers (q = quotient)
% @end
-module(qq_q).

-export_type([q/0]).

% TODO: reorganize like the complex number module is
-export([q0/0,
         q1/0,
         is_q/1,
         z2q/1,
         q2float/1,
         q/2,
         qplus/2,
         zplusq/2,
         qsum/1,
         qminus/2,
         qtimes/2,
         ztimesq/2,
         qdiv/2,
         qinv/1,
         qpow/2,
         pp/1,
         q2s/1,
         s2q/1]).


% main type
-record(q,
        {top :: qq_z:z(),
         bot :: qq_z:pn()}).
-type q() :: #q{}.

-type z() :: qq_z:z().


-spec q0() -> q().
q0() -> #q{top=0, bot=1}.


-spec q1() -> q().
q1() -> #q{top=1, bot=1}.


-spec is_q(term()) -> boolean().
is_q(#q{}) -> true;
is_q(_)    -> false.


-spec z2q(qq_z:z()) -> q().
z2q(Z) -> q(Z, 1).


-spec q2float(q()) -> float().
% @doc ugh
q2float({q, T, B}) -> T / B.


-spec q(qq_z:z(), qq_z:z()) -> q().
% @doc form the quotient of two integers
q(_, 0) ->
    erlang:error(divide_by_zero);
% sign goes on the top of the fraction
q(N1, N2) when N2 < 0 ->
    q(-1*N1, -1*N2);
% fraction stored in reduced format
q(0, _) ->
    #q{top=0, bot=1};
q(N, D) when N < 0 ->
    AbsQ = #q{top = T}
         = q(-1*N, D),
    Q = AbsQ#q{top = -1*T},
    Q;
% These two cases are not strictly needed, however, it means that the comment I
% write below explaining how to reduce fractions can be much shorter
q(1, D) -> #q{top=1, bot=D};
q(N, 1) -> #q{top=N, bot=1};
q(N, D) when N > 1, D > 1 ->
    % We want to reduce the fraction, in order to have unique representation.
    % The way to reduce a fraction is to divide both N and D by their GCD, and
    % store those two quantities in the #q record
    %
    % Facts:
    %   - N > 1
    %   - D > 1
    %   - the fraction N/D is reduced when N and D have no common divisors
    %   - any common divisor of N and D divides the greatest common divisor of
    %     N and D:
    %
    %       Proof:
    %           - let GCD = G = gcd(N, D)
    %           - (notation: X | Y means X divides Y, ie X rem Y = 0)
    %           - suppose Foo | N, Foo | D, but not(Foo | GCD). We will derive
    %             a contradiction by showing that this means GCD is not the
    %             gcd.
    %
    %
    %               - Write N, GCD, D, and Foo as a product of their prime
    %                 factors (Foo = F now)
    %
    %                   - N = PN1*PN2*PN3*PN4*...*PNMaxN
    %                   - D = PD1*PD2*PD3*PD4*...*PDMaxD
    %                   - G = PG1*PG2*PG3*PG4*...*PGMaxG = GCD
    %                   - F = PF1*PF2*PF3*PF4*...*PFMaxF = Foo
    %
    %               - All of these numbers are positive, so more factors =
    %                 greater
    %
    %               - (GCD|D and GCD|N) means that the factorization of GCD
    %                 appears as a subsequence of both the factorization of D
    %                 and the factorization of N
    %
    %                 To eliminate triviality about subsequences, you can
    %                 assume the factors are listed in non-descending order.
    %
    %               - Foo | D,N means that the factorization of Foo appears as
    %                 a subsequence in both the factorization of  D and the
    %                 factorization of N.
    %
    %               - not(Foo | GCD) means that the factorization of Foo is
    %                 not a subsequence of the factorization of GCD, although
    %                 it's possible that they share common factors.
    %
    %                 To eliminate the case where Foo and GCD share common
    %                 factors, we will divide them out
    %
    %               - Let Bar = Foo / (product of the non-descending sequence
    %                 of prime factors that Foo and GCD have in common)
    %
    %               - Bar | Foo by definition, and Foo | N,D, therefore
    %                 Bar|N,D
    %
    %               - The factorization of Bar*GCD appears as a subsequence of
    %                 both the factorization of N and D, therefore Bar*GCD |
    %                 N,D
    %
    %               - However, Bar*GCD > GCD, which contradicts the assumption
    %                 that GCD is the gcd
    %
    %               - QED
    %
    %   - That proof is not on 100% solid ground, because it assumes uniqueness
    %     of prime factorization; i.e. for any integer X greater than 1, there
    %     is a unique non-descending sequence of prime numbers whose product
    %     equals X.
    %
    %     That is true, but I did not prove that.
    %
    %   - Circling back, what this means is, if we want to reduce the fraction,
    %     then we only need to divide top and bottom by GCD, and then N and D
    %     are guaranteed to have no factors in common
    %
    %   - You should go read the comment in the gcd function to understand why
    %     that works.
    GCD = qq_z:gcd(N, D),
    Top = N div GCD,
    Bot = D div GCD,
    #q{top=Top, bot=Bot}.


-spec qplus(q(), q()) -> q().
qplus(#q{top=T1, bot=B1}, #q{top=T2, bot=B2}) ->
    % multiply Q1 * (B2/B2)
    %          Q2 * (B1/B1)
    %
    % To make common denominator
    q(T1*B2 + T2*B1, B1*B2).


-spec zplusq(z(), q()) -> q().
zplusq(Z, Q) ->
    qplus(z2q(Z), Q).


-spec qsum([q()]) -> q().
qsum(Qs) ->
    lists:foldl(
        fun(Q, Sum) ->
            qplus(Q, Sum)
        end,
        q0(),
        Qs).


-spec qminus(q(), q()) -> q().
qminus(Q1, Q2 = #q{top=T2}) ->
    % To make common denominator
    qplus(Q1, Q2#q{top = -1*T2}).


-spec qtimes(q(), q()) -> q().
qtimes(#q{top=T1, bot=B1}, #q{top=T2, bot=B2}) ->
    q(T1*T2, B1*B2).


-spec ztimesq(z(), q()) -> q().
ztimesq(N, Q) ->
    qtimes(z2q(N), Q).


-spec qdiv(q(), q()) -> q().
qdiv(#q{top=T1, bot=B1}, #q{top=T2, bot=B2}) ->
    q(T1*B2, B1*T2).


-spec qinv(q()) -> q().
% @doc 1/Q
qinv(Q) ->
    qdiv(q1(), Q).


-spec qpow(q(), qq_z:z()) -> q().
% @doc no way to recover a rational number from say 2^(1/2), so fractional
% powers are not allowed.
%
% Determining if an integer is a square requires factoring, and I'm not going
% to put factoring in the guard of a function head.
qpow(_, N) when not erlang:is_integer(N) ->
    erlang:error(qpow_non_integer_power);
qpow(_, 0) -> q1();
qpow(Q, P) when P < 0 ->
    qinv(qpow(Q, -1*P));
qpow(Q, P) when P > 0 ->
    qtimes(Q, qpow(Q, P-1)).


-spec pp(q()) -> ok.
pp(Q) -> io:format("~s~n", [q2s(Q)]).


-spec q2s(q()) -> string().
% @doc pretty printer
% @end
% whole number, just print it as an integer
q2s(#q{top=T, bot=1}) ->
    qq_z:z2s(T);
% fraction, print as Top/Bot
q2s(#q{top=T, bot=B}) ->
    TPP = qq_z:z2s(T),
    BPP = qq_z:z2s(B),
    TPP ++ "/" ++ BPP.


-spec s2q(string()) -> q().
% FIXME make it less joggerlicious, get rid of the if cancer
s2q(S) ->
    IsZstr = is_zstr(S),
    IsQstr = (not IsZstr) andalso is_qstr(S),
    if
        IsZstr ->
            Z = list_to_integer(S),
            z2q(Z);
        IsQstr ->
            [Top, Bot] = string:split(S, "/", leading),
            ZTop = erlang:list_to_integer(Top),
            ZBot = erlang:list_to_integer(Bot),
            q(ZTop, ZBot);
        true ->
            erlang:error({not_a_rational_str, S})
    end.


-spec is_zstr(string()) -> boolean().
is_zstr([$- | Rest]) ->
    is_pnstr(Rest);
is_zstr(S) ->
    is_pnstr(S).


-spec is_pnstr(string()) -> boolean().
is_pnstr([D]) ->
    lists:member(D, digit_chars());
is_pnstr([D | Rest]) ->
    lists:member(D, digit_chars()) andalso is_pnstr(Rest);
is_pnstr(_) ->
    false.


-spec digit_chars() -> string().
digit_chars() -> "0123456789".


-spec is_qstr(string()) -> boolean().
is_qstr(S) ->
    case string:split(S, "/", leading) of
        [Top, Bot] -> is_zstr(Top) andalso is_zstr(Bot);
        % No slash
        _          -> false
    end.


