% @doc
% This is a gen_server because I feel like writing object code. The reason is
% that the data structure is going to end up being quite complicated, and I
% want a simple declarative interface for it, that also validates the data I
% send it.
% @end
-module(hom).

-behavior(gen_server).

% api
-export(
    [ start_link/0
    , state/1
    ]
).
% defining bones
-export(
    [ defbone/4
    , defvertex/2
    , defedge/4
    , defface/3
    ]
).
%% getting bones
%-export(
%    [ defbone/4
%    , defvertex/2
%    , defedge/4
%    , defface/3
%    ]
%).
% gen_server
-export(
    [ init/1
    , handle_call/3
    , handle_cast/2
    ]
).


-type bone_name()   :: atom().
% ilc = integer linear combination, which we represent as a list
-type ilc(X)         :: ilcs:ilc(X).
% Really boundary should only contain signs + and - but eh this is good enough
% for now
-type bdy_list()     :: [{bone_name(), integer()}].
-type boundary()     :: ilc(bone_name()).
% Terminology: homology is defined on a skeleton, skeleton is a list of bones
-record(bone,
        { dim  :: non_neg_integer()
        , name :: bone_name()
          % boundary map, to each bone associate an integer linear combination
          % of other bones (which may be empty)
          %
          % For instance, an oriented edge going from a to b has boundary b-a.
          %
          % As a rule the boundary map is 2-nilpotent: which means that taking
          % the boundary of the boundary always gives 0. This needs to be
          % enforced by the private functions that are responsible for
          % maintaining this data structure.
        , bdy  :: boundary()
        }).
-type bone() :: #bone{}.
-record(s,
        { bones = [] :: [bone()]
        }).
-type state()   :: #s{}.

%-----------------------------------------------------------------------------%
% API: BASIC
%-----------------------------------------------------------------------------%

-spec start_link() -> Result
    when Result :: {ok, pid()}
                 | {error, Reason},
         Reason :: term().
start_link() ->
    gen_server:start_link(?MODULE, none, []).


% Barf the state, just for debugging.
-spec state(Self :: pid()) -> state().
state(Pid) ->
    gen_server:call(Pid, state).


%-----------------------------------------------------------------------------%
% API: CREATING THE STRUCTURE
%-----------------------------------------------------------------------------%

-spec defbone(Self, Dimension, Name, Boundary) -> Response
    when Self      :: pid(),
         Dimension :: non_neg_integer(),
         Name      :: bone_name(),
         Boundary  :: bdy_list(),
         Response  :: ok | {error, Reason},
         Reason    :: term().
defbone(Self, Dim, Name, Boundary) ->
    gen_server:call(Self, {defbone, Dim, Name, Boundary}).


-spec defvertex(Self, Name) -> Response
    when Self     :: pid(),
         Name     :: bone_name(),
         Response :: ok | {error, Reason},
         Reason   :: term().
defvertex(Self, Name) ->
    defbone(Self, 0, Name, []).


-spec defedge(Self, Name, Tail, Tip) -> Response
    when Self     :: pid(),
         Name     :: bone_name(),
         Tail     :: bone_name(),
         Tip      :: bone_name(),
         Response :: ok | {error, Reason},
         Reason   :: term().
defedge(Self, Name, Tail, Tip) ->
    defbone(Self, 1, Name, [{Tail, -1}, {Tip, 1}]).


-spec defface(Self, Name, Boundary) -> Response
    when Self     :: pid(),
         Name     :: bone_name(),
         Boundary :: bdy_list(),
         Response :: ok | {error, Reason},
         Reason   :: term().
defface(Self, Name, Boundary) ->
    defbone(Self, 2, Name, Boundary).


%-----------------------------------------------------------------------------%
% GEN_SERVER CALLBACKS
%-----------------------------------------------------------------------------%

-spec init(none) -> {ok, state()}.
init(none) ->
    State = #s{},
    {ok, State}.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, Reason},
         Reason   :: term(),
         NewState :: state().
handle_call({defbone, Dim, Name, Boundary}, _From, State) ->
    {Response, NewState} = do_defbone(Dim, Name, Boundary, State),
    {reply, Response, NewState};
% barfing state
handle_call(state, _From, State) ->
    {reply, State, State};
handle_call(Unexpected, From, State) ->
    ok = failblog("~p Unexpected call from ~tp: ~tp~n", [self(), From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
handle_cast(Unexpected, State) ->
    ok = failblog("~p Unexpected cast: ~tp~n", [self(), Unexpected]),
    {noreply, State}.

%-----------------------------------------------------------------------------%
% PRIVATE: DOERS
%-----------------------------------------------------------------------------%

% Really should factor out checking into a second function, because they will
% all have the same pattern
%
% - check if name is taken
% - check if boundary of boundary = 0
-spec do_defbone(Dimension, Name, BdyList , State) -> {Response, NewState}
    when Dimension :: non_neg_integer(),
         Name      :: bone_name(),
         BdyList   :: bdy_list(),
         State     :: state(),
         Response  :: ok | {error, Reason :: term()},
         NewState  :: state().
% This function does checks; once all the checks pass, add the skeleton
do_defbone(Dim, Name, BdyList, State = #s{bones=Bones}) ->
    Bdy = ilcs:from_list(BdyList),
    NewBone = #bone{ dim  = Dim
                   , name = Name
                   , bdy  = Bdy
                   },
    case bone_ok(BdyList, NewBone, State) of
        ok ->
            NewBones = [NewBone | Bones],
            NewState = State#s{bones = NewBones},
            {ok, NewState};
        Errors ->
            {Errors, State}
    end.

%-----------------------------------------------------------------------------%
% PRIVATE: HELPERS
%-----------------------------------------------------------------------------%

-spec bone_lookup(BoneName, Bones) -> {true, Bone} | false
    when BoneName :: bone_name(),
         Bones    :: [bone()],
         Bone     :: bone().
bone_lookup(_, []) ->
    false;
bone_lookup(BoneName, [Bone = #bone{name=BoneName} | _]) ->
    {true, Bone};
bone_lookup(BoneName, [_ | Rest]) ->
    bone_lookup(BoneName, Rest).


% print format message to stderr
-spec failblog(Str, Args) -> ok
    when Str  :: io:format(),
         Args :: [term()].
failblog(Str, Args) ->
    ok = io:format(standard_error, Str, Args),
    ok.


%-spec flat_format(Str, Args) -> string()
%    when Str  :: io:format(),
%         Args :: [term()].
%flat_format(Str, Args) ->
%    lists:flatten(io_lib:format(Str, Args)).

%-----------------------------------------------------------------------------%
% PRIVATE: VALIDATING NEW BONES
%-----------------------------------------------------------------------------%

-spec bone_ok(BdyList :: bdy_list(), PotentialBone :: bone(), State :: state()) -> Result
    when Result :: ok
                 | {errors, Errors},
         Errors :: [term()].
% @doc make sure potential bone is compatible with the state
%
%   - its name is available
%   - all of the names in its boundary exist
%   - [NYI] the boundary of its boundary is zero
bone_ok(BdyList, #bone{dim=Dim, name=Name, bdy=_Bdy}, #s{bones=Bones}) ->
    BdySymbols = [Symbol || {Symbol, _Coefficient} <- BdyList],
    Errors =
        % wrapping it in fun foo/1 is just so I don't have to turn off the
        % unused function warning
        assert_foldl(
            [ #{ this_fails_if => "trying to add a bone and there is another bone already in the skeleton with the same name."
               , fn            => fun name_taken/1
               , args          => {Name, Bones}
               , val           => name_available
               }
            , #{ this_fails_if => "trying to add a bone and there is some term in the boundary that has not been added to the skeleton."
               , fn            => fun boundary_symbols_exist/1
               , args          => {BdySymbols, Bones}
               , val           => all_exist
               }
            , #{ this_fails_if => "one of the boundary terms is of the wrong dimension"
               , fn            => fun boundary_terms_one_dim_lower/1
               , args          => {Dim, BdySymbols, Bones}
               , val           => all_one_dim_lower
               }
            ],
            []
        ),
    case Errors of
        []      -> ok;
        _Errors -> {errors, Errors}
    end.


% This doesn't work for some reason
-spec assert_foldl(Assertions, ErrorsAccum) -> Errors
    when Assertions  :: [Assertion],
         Assertion   :: #{atom() := term()},
         ErrorsAccum :: Errors,
         Errors      :: [term()].
% @doc sort of trying to mimick a custom monadfail from Haskell
% @end
% out of assertions, return ok
assert_foldl([], Accum) ->
    Accum;
assert_foldl([ThisAssertion = #{fn := Fn, args := Args, val := Val} | Rest], Accum) ->
    ActualVal = Fn(Args),
    ThisAssertionIsTrue = Val =:= ActualVal,
    NewAccum =
        case ThisAssertionIsTrue of
            true  -> Accum;
            false ->
                ErrorMessage = "an assertion failed",
                ErrorHash = #{message    => ErrorMessage
                             ,assertion  => ThisAssertion
                             ,actual_val => ActualVal
                             },
                [ErrorHash | Accum]
        end,
    assert_foldl(Rest, NewAccum).


% Might be faster to use lists:keyfind, but this is more obvious
-spec name_taken({Name, Bones}) -> name_available | name_taken
    when Name  :: bone_name(),
         Bones :: [bone()].
% @private
% determines if a name is taken or available. prevents adding bones with
% duplicate names
% @end
% No more names, it's available
name_taken({BoneName,Bones}) ->
    case bone_lookup(BoneName, Bones) of
        {true, _} -> name_taken;
        false     -> name_available
    end.
%name_taken({_, []})                        -> name_available;
%% Encountered a match, it's taken
%name_taken({Name, [#bone{name=Name} | _]}) -> name_taken;
%% No match, check the rest of the list
%name_taken({Name, [_ | Rest]})             -> name_taken({Name, Rest}).


-spec boundary_symbols_exist({BdySymbols, Bones}) -> Result
    when BdySymbols :: [bone_name()],
         Bones      :: [bone()],
         Result     :: all_exist
                     | {nonexistent_boundary_symbols, [bone_name()]}.
% @doc go through all the symbols in the boundary and make sure the vertex name
% exists
boundary_symbols_exist({BdySymbols, Bones}) ->
    case boundary_symbols_that_dont_exist(BdySymbols, Bones) of
        [] ->
            all_exist;
        DNEs ->
            {nonexistent_boundary_symbols, DNEs}
    end.


-spec boundary_symbols_that_dont_exist(BdySymbols, Bones) -> DNEs
    when BdySymbols :: [bone_name()],
         Bones      :: [bone()],
         DNEs       :: [bone_name()].
% No more bones left, return the accumulator.
boundary_symbols_that_dont_exist(BdySymbols, Bones) ->
    BdySymbolDNE =
        fun(Name) ->
            case name_taken({Name, Bones}) of
                % name is available iff the boundary symbol doesn't exist
                name_available -> true;
                % name is taken iff the boundary symbol exists
                name_taken     -> false
            end
        end,
    BdySymbolsThatDontExist = lists:filtermap(BdySymbolDNE, BdySymbols),
    BdySymbolsThatDontExist.

-spec boundary_terms_one_dim_lower({BoneDim, BdySymbols, Bones}) -> Result
    when BoneDim         :: non_neg_integer(),
         BdySymbols      :: ilc(bone_name()),
         Bones           :: [bone()],
         Result          :: all_one_dim_lower
                          | {counterexamples, Counterexamples},
         Counterexamples :: [term()].
boundary_terms_one_dim_lower({BoneDim,BdySymbols,Bones}) ->
    BoundaryDim = BoneDim - 1,
    AccumulateCounterexamples =
        % keeps a running list of bones that either don't exist or the
        % dimension doesn't match
        fun(BoneName, Counterexamples) ->
            NewCounterexamples =
                % Does bone even exist
                case bone_lookup(BoneName, Bones) of
                    % bone doesn't even exist
                    false ->
                        CE = #{ message   => "bone does not exist"
                              , bone_name => BoneName
                              },
                        NewCE = [CE | Counterexamples],
                        NewCE;
                    % bone exists
                    {true, #bone{dim=MatchDim}} ->
                        MatchIsCorrectDim = MatchDim =:= BoundaryDim,
                        % but does its dimension match
                        NewCE =
                            case MatchIsCorrectDim of
                                % dimension matches, no new counterexample
                                true  -> Counterexamples;
                                % dimension doesn't match, new counterexample
                                false ->
                                    CE = #{ message       => "dimension mismatch"
                                          , bone_name     => BoneName
                                          , dim_should_be => BoundaryDim
                                          , dim_is        => MatchDim
                                          },
                                    EvenNewerCE = [CE | Counterexamples],
                                    EvenNewerCE
                            end,
                        NewCE
                end,
            NewCounterexamples
        end,
    Counterexamples = lists:foldl(AccumulateCounterexamples, [], BdySymbols),
    case Counterexamples of
        []     -> all_one_dim_lower;
        Errors -> {counterexamples, Errors}
    end.
