% module for integer linear combinations (ilc)s
-module(ilcs).

-export_type(
    [ ilc/1
    ]
).

% Haskell clown world
%, ltstargt/2
-export(
    [ map/2
    , pure/1
    , puretimes/2
    , shove/2
    ]
).

% normal names
-export(
    [ coefficients/1
    , flatten/1
    , from_list/1
    , ilcplus/2
    , ilcsum/1
    , ilctimes/2
    , ilczero/0
    , is_ilc/1
    , symbols/1
    , to_list/1
    , to_map/1
    ]
).

-opaque ilc(X) :: #{X := integer()}.

%-----------------------------------------------------------------------------%
% Haskell clown world language
%
% The utility of the Haskell terminology is in telling is which operations are
% likely to be useful, because they're common enough to have a special idiom in
% the Haskell language.
%
% so we define
%
%   ilcs     | Haskell
%   --------------------
%   map      | fmap, <$>
%   pure     | pure, return
%            | <*>
%   shove    | =<<
%
% <*> is an operator that makes sense with Currying, which Erlang doesn't
% really have. If I need it I can add it, but as it stands it's just more bug
% surface.
%-----------------------------------------------------------------------------%

-spec map(fun((X) -> Y), ilc(X)) -> ilc(Y)
    when X :: term(),
         Y :: term().
map(X2Y, ILCX) ->
    maps:fold(
        fun(X, N, Accum) ->
            Y = X2Y(X),
            PureY = pure(Y),
            NY = ilctimes(N, PureY),
            ilcplus(NY, Accum)
        end,
        ilczero(),
        ILCX
    ).


-spec pure(X) -> ilc(X) when X :: term().
pure(X) -> from_list([{X, 1}]).


-spec puretimes(N, X) -> ilc(X)
    when N :: integer(),
         X :: term().
puretimes(N, X) -> from_list([{X, N}]).



-spec shove(LinMap, ILCX) -> ILCY
    when LinMap :: fun((X) -> ilc(Y)),
         ILCX   :: ilc(X),
         ILCY   :: ilc(Y),
         X      :: term(),
         Y      :: term().
% @doc
% Apply a linear map to each symbol in an integer linear combination
%
% Linear maps have the property
%
%   f(A*x + By) = A*f(x) + B*f(y)
%
% So for our application, let us say that we are computing the boundary map:
%
%   bdy(A*edge1 + B*edge2) = A*bdy(edge1) + B*bdy(edge2)
%
% the bdy map can be directly defined in terms of the data structure on edge1
% and edge2. This function provides a ``lifting'' (of sorts) from the map
% defined on symbols to defining a map on linear combinations
%
% It's conceptually identical to the shove operator from Haskell, just with the
% order of operands reversed:
%
%   Prelude> :t (=<<)
%   (=<<) :: Monad m => (a -> m b) -> m a -> m b
%
% where ilc is our monad
% @end
shove(XToILCY, XIntMap) ->
    maps:fold(
        fun(X, N, Accum) ->
            ILCY = XToILCY(X),
            NILCY = ilctimes(N, ILCY),
            ilcplus(NILCY, Accum)
        end,
        ilczero(),
        XIntMap
    ).


%-----------------------------------------------------------------------------%
% API that has normal names
%-----------------------------------------------------------------------------%

-spec coefficients(ilc(term())) -> [integer()].
coefficients(ILC) -> maps:values(ILC).


-spec flatten(ILCS) -> ILC
    when ILCS :: ilc(X)
               | ilc(ILCS),
         ILC  :: ilc(X),
         X    :: term().
% takes an ILC-of-ILCs and converts it to a flat ILC
flatten(ILC) ->
    maps:fold(
        fun(MaybeILC, N, Accum) ->
            case is_ilc(MaybeILC) of
                % this is some other type of term, leave it
                false ->
                    %io:format(
                    %    "false branch~n"
                    %    "   ILC      = ~p~n"
                    %    "   MaybeILC = ~p~n"
                    %    "   N        = ~p~n"
                    %    "   Accum    = ~p~n",
                    %    [ILC, MaybeILC, N, Accum]
                    %),
                    Term = puretimes(N, MaybeILC),
                    ilcplus(Term, Accum);
                % this term is itself an integer-linear-combination; it might
                % be recursive, so flatten IT, then construct a term
                true ->
                    %io:format(
                    %    "true branch~n"
                    %    "   ILC      = ~p~n"
                    %    "   MaybeILC = ~p~n"
                    %    "   N        = ~p~n"
                    %    "   Accum    = ~p~n",
                    %    [ILC, MaybeILC, N, Accum]
                    %),
                    FlatTerm = ilctimes(N, flatten(MaybeILC)),
                    ilcplus(FlatTerm, Accum)
            end
        end,
        ilczero(),
        ILC
    ).


-spec from_list([{X, integer()}]) -> ilc(X)
    when X :: term().
from_list(Pairs) ->
    % not a straightforward call to maps:from_list because for instance
    %
    % 2> maps:from_list([{x, 1}, {x, 2}]).
    % #{x => 2}
    %
    % We want that to be #{x => 3}
    from_list2(Pairs, #{}).


-spec from_list2(Pairs, Accum) -> ILC
    when Pairs :: [{X, integer()}],
         Accum :: ilc(X),
         ILC   :: ilc(X),
         X     :: term().
from_list2([], Accum) ->
    Accum;
from_list2([{X, N} | Pairs], Accum) ->
    NewAccum = ilcplus(#{X => N}, Accum),
    from_list2(Pairs, NewAccum).



% Add two integer linear combinations
%
%
% 7> ilcs:from_list([{x, 1}, {x, 1}]).
% #{x => 2}
% 8> ilcs:from_list([{x, 1}, {x, -1}]).
% #{}
% 9> ilcs:from_list([{x, 1}, {y, 1}]).
% #{x => 1,y => 1}
% 10> ilcs:from_list([{x, 1}, {y, 1}, {x, -1}]).
% #{y => 1}
-spec ilcplus(ilc(X), ilc(X)) -> ilc(X)
    when X :: term().
ilcplus(A, B) ->
    KeysA = maps:keys(A),
    KeysB = maps:keys(B),
    KeysAMinusB = KeysA -- KeysB,
    % A \ (A \ B) = A intersect B
    % A \ B       = set of points in A not in B
    % A \ (A \ B) = set of points in A also in B
    KeysAIntersectB = KeysA -- KeysAMinusB,
    KeysBMinusA = KeysB -- KeysA,
    % Build an ILC first from the keys that only appear in one
    % start with just the keys that are in A
    ILCFromKeysThatAreOnlyInA =
        lists:foldl(
            fun(Key, AccumMap) ->
                Val = maps:get(Key, A),
                NewAccum = maps:put(Key, Val, AccumMap),
                NewAccum
            end,
            #{},
            KeysAMinusB
        ),
    % Now add in the keys that are only in B
    ILCFromKeysThatAreOnlyInEither =
        lists:foldl(
            fun(Key, AccumMap) ->
                Val = maps:get(Key, B),
                NewAccum = maps:put(Key, Val, AccumMap),
                NewAccum
            end,
            ILCFromKeysThatAreOnlyInA,
            KeysBMinusA
        ),
    % Add in finally the keys that are in both
    % For the keys that appear in both, we have to take the sum
    ILCFinal =
        lists:foldl(
            fun(Key, AccumMap) ->
                ValA = maps:get(Key, A),
                ValB = maps:get(Key, B),
                NewAccumMap =
                    case ValA + ValB of
                        % if they sum to 0, don't add an entry to the map
                        0   -> AccumMap;
                        Val -> maps:put(Key, Val, AccumMap)
                    end,
                NewAccumMap
            end,
            ILCFromKeysThatAreOnlyInEither,
            KeysAIntersectB
        ),
    ILCFinal.


-spec ilcsum([ilc(X)]) -> ilc(X) when X :: term().
ilcsum(Items) ->
    lists:foldl(
        fun(Item, Accum) ->
            ilcplus(Item, Accum)
        end,
        ilczero(),
        Items
    ).


-spec ilctimes(integer(), ilc(X)) -> ilc(X) when X :: term().
ilctimes(0, _) -> #{};
ilctimes(N, ILC) ->
    maps:map(
        fun(_Symbol, Coeff) ->
            N*Coeff
        end,
        ILC
    ).


-spec ilczero() -> ilc(X :: term()).
% @doc null ilc, doesn't matter what type
ilczero() -> #{}.


-spec is_ilc(term()) -> boolean().
% @doc something is an ILC if it is a map and all values are integers
is_ilc(Foo) ->
    erlang:is_map(Foo)
        andalso
            lists:all(
                fun(Coefficient) ->
                    erlang:is_integer(Coefficient)
                end,
                coefficients(Foo)
            ).


-spec symbols(ilc(X)) -> [X] when X :: term().
% @doc the things that we are taking linear combinations of
symbols(ILC) -> maps:keys(ILC).


-spec to_list(ilc(X)) -> [{X, integer()}] when X :: term().
% @doc alias for maps:to_list/1 function for now
to_list(ILC) -> maps:to_list(ILC).


-spec to_map(ilc(X)) -> #{X := integer()} when X :: term().
% @doc identity function for now
to_map(ILC) -> ILC.
