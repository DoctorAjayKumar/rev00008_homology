#!/usr/bin/env escript

-mode(compile).

main([]) ->
      {ok, Skel} = hom:start_link()
    , ok = hom:defvertex(Skel, v1)
    , ok = hom:defvertex(Skel, v2)
    , ok = hom:defedge(Skel, e1, v1, v2)
    , ok = hom:defedge(Skel, e2, v1, v1)
    , ok = hom:defedge(Skel, e3, e1, e1)
    , ok = io:format("~p~n", [hom:state(Skel)])
    .
    %, io:format("~p~n", [Foo])
